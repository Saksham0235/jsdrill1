import {inventory} from './inventory.js'

// #Solution 1

// function findcarbyid(id)
// {
//     for(let i=0;i<inventory.length;i++)
//     {
//         if(inventory[i].id===id)
//         {
//             return inventory[i];
//         }
//     }
//     return null;
// }


// const car20 =findcarbyid(20);

// if(car20)
// {
//     console.log(`Car is a ${car20.car_year} ${car20.car_make} ${car20.car_model}`)
// }
// else{
//     console.log(`Car doesnot find in the book log`)
// }




// #Solution 2 

// function lastcar()
// {
//     console.log(`Last car is a ${inventory[inventory.length-1].car_make} By ${inventory[inventory.length-1].car_model}`)
// }

// lastcar();


// #Solution 3

function carsinsorted()
{
    const carModels=[];
    for(let i=0;i<inventory.length-1;i++)
    {
        carModels.push(inventory[i].car_model);
    }

    carModels.sort();

    return carModels;
}

// const cars=carsinsorted();

// console.log(`Cars in sorted order`);
// console.log(cars);

// #Solution 4

function carsyears()
{
    const carModels=[];
    for(let i=0;i<inventory.length-1;i++)
    {
        carModels.push(inventory[i].car_year);
    }

    return carModels;
}

// const caryears=carsyears();

// console.log(`Cars years: ${caryears}`)



// #Solution 5 

// const carsolder2000=carsyears();

// function Carsold()
// {
//     const arr=[];
//     for(let i=0;i<carsolder2000.length;i++)
//     {
//         if(carsolder2000[i]>=2000)
//         {
//             arr.push(carsolder2000[i]);
//         }
//     }
//     return arr;

// }

// const carsarr=Carsold();

// console.log(`Cars older than 2000 are ${carsarr.length}`)


// #Solution 6 
const bmw=carsinsorted();

function Carsbmw()
{
   const cararr=[];
   for(let i=0;i<inventory.length;i++)
   {
    const car=inventory[i];
    if(car.car_make==='BMW' || car.car_make==='Audi')
    {
        cararr.push(car);
    }
   }
   return cararr;

}

const bmwcars=Carsbmw();
// #JSON.stringify() is used to convert the object into json formatted string.
console.log(JSON.stringify(bmwcars));





